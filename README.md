This tool will be a confirmation page template package for the uw_conditional_rulesets module; it is dependent on this module. 

The WF1 template uses a for-loop that displays all results for webform questions, in conjuction with using rulesets and conditionals.
It displays all results that process as true, this means a constant amount of results will always display on the page. 
For our webform, this template was used for displaying the results from six scenarios. Each question in these scenarios had three answer options: yes, no, and maybe. 
Through conditionals, we assigned an employability skill to populate under one of three headings (high, medium, and low priority), depending on user answers. 
For our webform, WF1 template ensured all 12 skills (results) would show up under the proper categories. 
In addition, the loop allowed for a dynamic list with no set amount of results; which allowed for many more permutuations of the confirmation page to occur, based off user answers.


The WF2 template was used for our webform that displayed on-campus resources based off user answers. User picked an employablity skill to develop and all resources related to that would be populated.
However, we set the page to only show 6 results at once, with the option to refresh results if users did not find what they were looking for.
This template utilizes the ruleset categories as headings, for example, "Learn the Skill." The conditionals acts as criteria for the user to get relevant resources.